from twisted.application import internet, service
from network.server_core import RootFactory
from txws import WebSocketFactory
import logging
from root_manager import RootManager
from twisted.enterprise import adbapi
import db.db_keep_active as dbKeepActive


logging.basicConfig(
    filename='../log_server_paths.txt',
    level=logging.DEBUG,
    format='%(asctime)s - %(levelname)s - %(message)s')

dbpool = adbapi.ConnectionPool(
    "MySQLdb",
    db="Paths",
    user='root',
    passwd='piO',
    host='185.133.194.162',
    cp_reconnect=True)

logging.debug('~   ~  ~ ~App started.')
print "                          (\_/)"
print "                          (^_^)"
print "... environment started. (_____)O"


root_f = RootFactory(RootManager(dbpool))

application = service.Application("paths")

socketService = internet.TCPServer(8018, root_f)
socketService.setServiceParent(application)

webSocketService = internet.TCPServer(8017, WebSocketFactory(root_f))
webSocketService.setServiceParent(application)

dbPingService = dbKeepActive.SqlPingService(dbpool)
dbPingService.setServiceParent(application)
