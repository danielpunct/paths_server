from command_dispatcher import OUTRoutes, InRoutes, COMM_send
import db.db_handler as DB
from network.network_listener import FuncSync


@FuncSync(InRoutes.chat_newMessage.name)
def on_new_message(client_manager, json_text):
    sender = client_manager.get_display_name()
    sender_id = client_manager.get_functional_id()

    root_manager = client_manager.root_manager
    COMM_send(
        root_manager.clients.values(),
        OUTRoutes.chat_newMessage.name,
        {'sender': sender, 'text': json_text})
    DB.store_message(root_manager.db_pool, json_text, sender_id, 0)


def send_history(client_manager):
    # receive last 50 messages
    dff = DB.messages_get_last50(client_manager.root_manager.db_pool)

    def cbk_get_last50(results):
        COMM_send(
            (client_manager,),
            OUTRoutes.chat_getLast50.name,
            results)

    dff.addCallback(cbk_get_last50)
