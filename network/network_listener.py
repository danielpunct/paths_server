from types import FunctionType


class NetworkManager(object):
    listeners = []

    def __init__(self):
        decorated_methods = [
             y for x, y in self.__class__.__dict__.items()
             if type(y) == FunctionType and hasattr(y, 'arg_route')]
        print decorated_methods
        for meth in decorated_methods:
            my_route = getattr(meth, 'arg_route').route
            self.listeners.append(Listener(my_route, meth, self))

    @classmethod
    def resolve_message(self, event_route, client_manager, data_json):
        for listener in self.listeners:
            if listener.route == event_route:
                if listener.child:
                    listener.callback(listener.child, client_manager, data_json)
                else:
                    listener.callback(client_manager, data_json)


class Listener:
    def __init__(self, route, callback, child_instance=None):
        self.route = route
        self.callback = callback
        self.child = child_instance


class MethSync(object):
    ''' Methods Decorator '''

    def __init__(self, route):
        self.route = route

    def __call__(self, f):
        f.arg_route = self
        return f


class FuncSync(object):
    ''' Decorator for static functions '''

    def __init__(self, route):
        self.route = route

    def __call__(self, f):
        f.arg_route = self
        NetworkManager.listeners.append(Listener(self.route, f))
        return f