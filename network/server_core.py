from twisted.internet.protocol import Factory, Protocol
import command_dispatcher
import logging


class ClientProtocol(Protocol):

    #  a new client connects...
    def connectionMade(self):
        self.transport.setTcpNoDelay(True)
        self.root_manager = self.factory.root_manager
        self.client_manager = self.root_manager.connectClient(self)
        # log
        logging.debug(
                '-- connected from ' + self.transport.getPeer().host
                + ' with index ' + str(self.client_manager.client_index))

    def connectionLost(self, reason):
        self.root_manager.disconnectClient(self.client_manager)
        # log
        logging.debug('-- disconnected from ' + self.transport.getPeer().host)

    def dataReceived(self, data):
        if data == 'disconnect':
            self.transport.loseConnection()
        else:
            command_dispatcher.execute_IN_message(data, self.client_manager)


class RootFactory(Factory):

    # This will be used by the default buildProtocol to create new protocols:
    protocol = ClientProtocol

    def __init__(self, rootManager):
        self.root_manager = rootManager
