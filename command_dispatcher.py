import json
import logging
from network.network_listener import NetworkManager
from enum import Enum


def execute_IN_message(json_string, client_manager):
    logging.debug('[rm< intaint ] ' + json_string)
    mesages = filter(None, json_string.split('>|<'))
    for message in mesages:
        logging.debug('[rm< partial] ' + message)
        json_dict = json.loads(message)
        k = json_dict.keys()[0]
        NetworkManager.resolve_message(k, client_manager, json_dict[k])


def COMM_send(clients, route, args_list):
    logging.debug('[rm>] ' + route)
    data = {'route': route, 'value': args_list}
    for client in clients:
        client.tcp_protocol.transport.write('>|<' + json.dumps(data) + '>|<')


class OUTRoutes(Enum):
    game_greet = "game_greet"

    chat_newMessage = "chat_newMessage"
    chat_getLast50 = "chat_getLast50"

    login_userFound = "login_userFound"
    login_userNotFound = "login_userNotFound"
    login_userRegistered = "login_userRegistered"
    login_sendUsersList = "login_sendUsersList"

    lobby_sendClientsList = "lobby_sendClientsList"
    lobby_inviteDuel = "lobby_inviteDuel"
    lobby_startGame = "lobby_startGame"
    lobby_denyDuel = "lobby_denyDuel"
    lobby_updateState = "lobby_updateState"

    battle_startBattle = "battle_startBattle"
    battle_newTroop = "battle_newTroop"
    battle_lockTroop = "battle_lockTroop"
    battle_takeDamage = "battle_takeDamage"
    battle_battle_over = "battle_battle_over"
    battle_package = "battle_package"

    battle_game_over = "battle_game_over"


class InRoutes(Enum):
    chat_newMessage = "chat_newMessage"

    login_tryLogin = "login_tryLogin"
    login_tryRegister = "login_tryRegister"

    lobby_backToLobby = "lobby_backToLobby"
    lobby_inviteDuel = "lobby_inviteDuel"
    lobby_acceptDuel = "lobby_acceptDuel"
    lobby_denyDuel = "lobby_denyDuel"

    battle_lockTroop = "battle_lockTroop"
    battle_newTroop = "battle_newTroop"
    battle_readyBattle = "battle_readyBattle"
    battle_takeDamage = "battle_takeDamage"
    battle_battle_over = "battle_battle_over"

    battle_package = "battle_package"

    battle_game_over = "battle_game_over"
