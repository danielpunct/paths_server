from command_dispatcher import OUTRoutes, InRoutes, COMM_send
from battle import battle_dispatcher
import chat_manager
from network.network_listener import FuncSync


def sendClientsList(root_manager, destination_clients):
    online_clients = root_manager.clients.values()
    COMM_send(
        destination_clients,
        OUTRoutes.lobby_sendClientsList.name,
        [{'clientIdx': client.client_index,
          'name': client.get_display_name(),
          'state': client.avatar.state} for client in online_clients])


def sendStatusUpdate(root_manager, updated_client_idxs, new_status):
    COMM_send(
        root_manager.clients.values(),
        OUTRoutes.lobby_updateState.name,
        {'clientIdxs': updated_client_idxs,
         'state': new_status})


@FuncSync(InRoutes.lobby_backToLobby.name)
def on_back_to_lobby(client_manager, no_data):
    # end battle for this one
    client_manager.battle = None
    client_manager.avatar.state = 'idle'
    # send data
    sendClientsList(
        client_manager.root_manager,
        (client_manager,))
    chat_manager.send_history(client_manager)
    sendStatusUpdate(
        client_manager.root_manager,
        (client_manager.client_index,),
        'idle')


@FuncSync(InRoutes.lobby_inviteDuel.name)
def on_lobby_duel_invite(client_A, json_client_B_idx):
    COMM_send(
        (client_A.root_manager.clients[int(json_client_B_idx)],),
        OUTRoutes.lobby_inviteDuel.name,
        client_A.client_index)


@FuncSync(InRoutes.lobby_acceptDuel.name)
def send_start_game(client_B, data_json):
    client_A = client_B.root_manager.clients[int(data_json)]
    battle_dispatcher.start_new_battle(client_A, client_B)

    COMM_send(
        (client_A,),
        OUTRoutes.lobby_startGame.name,
        client_B.client_index)
    # i should refactor and this
    COMM_send(
        (client_B,),
        OUTRoutes.lobby_startGame.name,
        client_A.client_index)


@FuncSync(InRoutes.lobby_denyDuel)
def send_deny_duel(client_B, data_json):
    client_A = client_B.root_manager.clients[int(data_json)]

    COMM_send(
        (client_A,),
        OUTRoutes.lobby_denyDuel.name,
        client_B.client_index)
