import json
from network.network_listener import FuncSync
from command_dispatcher import InRoutes
from twisted.cred import credentials
from zope.interface import implements, Interface
import hashlib
import db.db_handler as DB


def hash(password):
    return hashlib.md5(password).hexdigest()


@FuncSync(InRoutes.login_tryLogin.name)
def tryLogin(client_manager, login_data):
    data_json = json.loads(login_data)  # extra parse json because extra
    username = data_json['name']
    password = data_json['pass']
    client_manager.portal.login(
        credentials.UsernameHashedPassword(username,
                                           hash(password)),
        None,
        IProtocolAvatar).addCallbacks(client_manager._cbLogin,
                                      client_manager._ebLogin)


@FuncSync(InRoutes.login_tryRegister.name)
def tryRegister(client_manager, login_data):
    data_json = json.loads(login_data)  # extra parse json because extra
    username = data_json['name']
    password = data_json['pass']
    dff = DB.get_user(client_manager.db_pool, username)

    def cbk_search_existent(results):
        if int(results[0][0]) > 0:
            client_manager._ebLogin("User found")
        else:
            dffOk = DB.register_user(client_manager.db_pool, username, password)
            dffOk.addCallback(cbk_register)

    dff.addCallback(cbk_search_existent)

    def cbk_register(results):
        client_manager.portal.login(
            credentials.UsernameHashedPassword(username,
                                               hash(password)),
            None,
            IProtocolAvatar).addCallbacks(client_manager._cbLogin,
                                          client_manager._ebLogin)


class IProtocolAvatar(Interface):
    def logout():
        """
        Clean up per-login resources allocated to this avatar.
        """


class User(object):
    implements(IProtocolAvatar)

    def logout(self):
        pass

    def __init__(self, name):
        self.name = name
        self.user_id = None
        self.state = 'idle'  # can be battle, idle
