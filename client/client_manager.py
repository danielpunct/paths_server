from command_dispatcher import OUTRoutes, COMM_send
import db.db_handler as DB
import lobby_manager
from client_dispatcher import User


class ClientManager:
    def __init__(self, root_manager, client_index, tcp_protocol, myPortal):
        self.battle = None
        self.db_pool = root_manager.db_pool
        self.root_manager = root_manager
        self.client_index = client_index
        self.tcp_protocol = tcp_protocol

        self.portal = myPortal
        self.avatar = User('noname')
        self.avatar.user_id = -1
        self.logout = None

    def _cbLogin(self, (interface, avatar, logout)):
        self.avatar = avatar
        self.logout = logout

        dff = DB.get_user_data(self.db_pool, self.avatar.name)

        def cbk_user_data(results):
            self.avatar.user_id = results[0][1]

            lobby_manager.sendClientsList(
                self.root_manager,
                self.root_manager.clients.values())
            COMM_send(
                (self,),
                OUTRoutes.login_userFound.name,
                {'user': self.avatar.name})

        dff.addCallback(cbk_user_data)

    def _ebLogin(self, failure):
        userFound = False
        if 'User not in database' in str(failure):
            userFound = True
        # todo send message need to be registered
        COMM_send(
            (self,),
            OUTRoutes.login_userNotFound.name,
            {"userFound": "no" if userFound else "yes",
             "error": str(failure)})

    def get_display_name(self):
        sender = ''
        if self.avatar and self.avatar.user_id >= 0:
            sender = self.avatar.name
        else:
            sender = str(self.client_index)
        return sender

    ''' for sql storage '''
    def get_functional_id(self):
        sender_id = 0
        if self.avatar and self.avatar.user_id >= 0:
            sender_id = self.avatar.user_id
        else:
            sender_id = -self.client_index
        return sender_id
