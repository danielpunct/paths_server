from twisted.internet import task
from twisted.application import service
import logging


class SqlPingService(service.Service):
    def __init__(self, db_pointer):
        self.dbpool = db_pointer
        self.dbping = task.LoopingCall(self.dbping)

        self.reconnect = False
        logging.debug('database ping initiated')

    def startService(self):
        # 20 minutes = 1200 seconds; if MySQL socket is
        # idled for 20 minutes or longer, MySQL itself disconnects
        self.dbping.start(1200)

    def stopService(self):
        pass

    def dbping(self):
        def ping(conn):
            # i can chose to use the native command
            # instead of sending null commands to the database.
            conn.ping()
        pingdb = self.dbpool.runWithConnection(ping)
        pingdb.addCallback(self.dbactive)
        pingdb.addErrback(self.dbout)
        # print "pinging database"

    def dbactive(self, data):
        if data is None and self.reconnect is True:
            self.dbping.stop()
            self.reconnect = False
            self.dbping.start(1200)  # 20 minutes = 1200 seconds
            # print "Reconnected to database!"
        elif data is None:
            # print "database is active"
            pass

    def dbout(self, deferr):
        # print deferr
        if self.reconnect is False:
            self.dbreconnect()
        elif self.reconnect is True:
            logging.debug('Unable to reconnect to database')
        logging.debug('unable to ping MySQL database!')

    def dbreconnect(self, *data):
        self.dbping.stop()
        self.reconnect is True
        # self.dbping = task.LoopingCall(self.dbping)
        self.dbping.start(60)  # 60
