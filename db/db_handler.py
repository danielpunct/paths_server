import hashlib


def hash(password):
    return hashlib.md5(password).hexdigest()


# QUERRYES
# USERS
def get_user(dbpool, name):
    return dbpool.runQuery(
        "SELECT COUNT(username) FROM app_users WHERE username = %s",
        (name,))


def register_user(dbpool, name, password):
    return dbpool.runQuery(
        "INSERT INTO app_users (username, password) VALUES ((%s), (%s))",
        (name, hash(password)))


def get_user_data(dbpool, name):
    return dbpool.runQuery(
        "SELECT username, id FROM app_users WHERE username = %s",
        (name,))


# MESSAGES
def store_message(dbpool, text, sender_id, destination_id):
    dbpool.runQuery(
        "INSERT INTO chat_messages (sender_id, text, time, lobby_id) VALUES ((%s), (%s), NOW(), (%s))",
        (sender_id, text, destination_id))


def messages_get_last50(dbpool):
    return dbpool.runQuery('''SELECT CASE WHEN Messages.sender_id < 0
            THEN -Messages.sender_id
            ELSE (SELECT Users.username FROM app_users as Users WHERE Users.id = Messages.sender_id )
            END AS sender, Messages.text FROM chat_messages as Messages ORDER BY Messages.id DESC LIMIT 30''')
