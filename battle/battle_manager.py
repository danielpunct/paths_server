

class BattleManager:

    def __init__(self, client_manager_A, client_manager_B):
        self.client_A = client_manager_A
        self.client_B = client_manager_B
        self.client_A_ready = False
        self.client_B_ready = False
        self.start_time = None
        client_manager_A.battle = self
        client_manager_B.battle = self
        self.troop_idx = 0

    'not to be used'
    def place_new_troop(self, client_manager):
        side = "A" if self.client_A == client_manager else "B"
        self.troop_idx += 1
        return {'side': side, 'troopIdx': self.troop_idx}
