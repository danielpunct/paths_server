import datetime
import json
from command_dispatcher import OUTRoutes, InRoutes, COMM_send
from network.network_listener import FuncSync
from battle import battle_manager
import lobby_manager


def start_new_battle(client_A, client_B):
    battle_manager.BattleManager(
        client_A,
        client_B)
    client_A.avatar.state = 'battle'
    client_B.avatar.state = 'battle'
    lobby_manager.sendStatusUpdate(
        client_A.root_manager,
        (client_A.client_index, client_B.client_index),
        'battle')


@FuncSync(InRoutes.battle_package.name)
def send_packagedata(client_manager, packageJson):
    battle = client_manager.battle
    other_client = battle.client_A if client_manager == battle.client_B else battle.client_B
    new_troop = battle.place_new_troop(client_manager)
    COMM_send(
        (other_client,),
        OUTRoutes.battle_package.name,
        json.loads(packageJson))

@FuncSync(InRoutes.battle_readyBattle.name)
def client_is_ready(client_manager, no_data):
    battle = client_manager.battle
    if battle.client_A == client_manager:
        battle.client_A_ready = True
    elif battle.client_B == client_manager:
        battle.client_B_ready = True

    if battle.client_A_ready and battle.client_B_ready:
        battle.start_time = get_time_stamp()
        COMM_send(
            (battle.client_A,),
            OUTRoutes.battle_startBattle.name,
            {'side': 'A', 'start_time': battle.start_time})
        COMM_send(
            (battle.client_B,),
            OUTRoutes.battle_startBattle.name,
            {'side': 'B', 'start_time': battle.start_time})


'to delete'
def send_new_troop(client_manager, positionsJson):
    battle = client_manager.battle
    new_troop = battle.place_new_troop(client_manager)
    COMM_send(
        (battle.client_A, battle.client_B),
        OUTRoutes.battle_newTroop.name,
        {'side': new_troop['side'],
         'positions': positionsJson,
         'time': get_time_stamp(),
         'idx': new_troop['troopIdx']})


'to delete'
def send_battle_over(client_manager, dataJson):
    data_json = json.loads(dataJson)  # extra  parse from json because extra
    battle = client_manager.battle

    COMM_send(
        (battle.client_A, battle.client_B),
        OUTRoutes.battle_battle_over.name,
        {'troopId': data_json['troopId'],
         'time': get_time_stamp()})


'to delete'
def send_lock_troop(client_manager, dataJson):
    data_json = json.loads(dataJson)  # extra parse from json because extra
    battle = client_manager.battle

    COMM_send(
        (battle.client_A, battle.client_B),
        OUTRoutes.battle_lockTroop.name,
        {'troopId': data_json['troopId'],
         'destId': data_json['destinationId'],
         'time': get_time_stamp()})


'to delete'
def send_take_damage(client_manager, data_json):
    battle = client_manager.battle

    COMM_send(
        (battle.client_A, battle.client_B),
        OUTRoutes.battle_takeDamage.name,
        {'damages': data_json,
         'time': get_time_stamp()})


'to delete'
def game_is_over(client_manager, winning_side):
    battle = client_manager.battle
    winning_client = battle.client_A if winning_side == 'a' else battle.client_B
    COMM_send(
        (battle.client_A, battle.client_B),
        OUTRoutes.battle_game_over.name,
        {'winName': winning_client.get_display_name()})


def enco(obj):
    return (
        obj.isoformat()
        if isinstance(obj, datetime.datetime)
        or isinstance(obj, datetime.date)
        else None)


def get_time_stamp():
    return json.dumps(datetime.datetime.now(), default=enco)
