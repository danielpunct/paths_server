from client.client_manager import ClientManager
from client.client_dispatcher import User, IProtocolAvatar
import lobby_manager
from command_dispatcher import OUTRoutes, COMM_send
from zope.interface import implements
from twisted.cred import portal
from db.db_checker import DBCredentialsChecker


class RootManager:
    def __init__(self, dbPool):
        self.db_pool = dbPool
        self.clients = {}
        self.client_index = 0
        checker = DBCredentialsChecker(
            dbPool.runQuery,
            query="SELECT username, password FROM app_users WHERE username = %s")

        myPortal.registerChecker(checker)

    def connectClient(self, tcp_protocol):
        self.client_index += 1
        client = ClientManager(self, self.client_index, tcp_protocol, myPortal)
        self.clients[self.client_index] = client
        self.sendGreet(client)
        lobby_manager.sendClientsList(
            self,
            self.clients.values())
        return client

    def disconnectClient(self, client_manager):
        del self.clients[client_manager.client_index]
        lobby_manager.sendClientsList(
            self,
            self.clients.values())

    # helper
    def sendGreet(self, client):
        COMM_send(
            (client,),
            OUTRoutes.game_greet.name,
            {'message': 'server greets you', 'id': client.client_index})


# CREDENTIALS

class Realm(object):
    implements(portal.IRealm)

    def requestAvatar(self, avatarId, mind, *interfaces):
        if IProtocolAvatar in interfaces:
            avatar = User(avatarId)
            return IProtocolAvatar, avatar, avatar.logout
        raise NotImplementedError(
            "This realm only supports the IProtocolAvatar interface.")


realm = Realm()
myPortal = portal.Portal(realm)
